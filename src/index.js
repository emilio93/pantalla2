import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

const contactList = [
  {
    name: "Juan",
    lastName: "Ramirez",
    age: 23,
    email: "juan@ramirez.com",
    country: "Costa Rica"
  }, {
    name: "Pedro",
    lastName: "Montero",
    age: 43,
    email: "pedro@montero.com", country: "Costa Rica"
  },
  {
    name: "Keylor",
    lastName: "Jimenez",
    age: 36,
    email: "keylor@jimenez.com",
    country: "Costa Rica"
  }, {
    name: "Andres",
    lastName: "Guillen",
    age: 12,
    email: "andres@guillen.com",
    country: "Costa Rica"
  }, {
    name: "Maria",
    lastName: "Alvarado",
    age: 30,
    email: '',
    country: "Costa Rica"
  }, {
    name: "Ana",
    lastName: "Palermo",
    age: 29,
    email: "ana@palermo.com",
    country: "Costa Rica"
  }, {
    name: "Sabrina",
    lastName: "Fernandez",
    age: 54,
    email: "sabrina@fernandez.com",
    country: "Costa Rica"
  }, {
    name: "Lila",
    lastName: "Ramirez",
    age: 24,
    email: "lila@ramirez.com",
    country: ''
  },
];

function Contact({ name, lastName, age, email, country }) {
  return (
    <div>
      <span><b>Name</b>: {name}</span>
      <br />
      <span><b>Last Name</b>: {lastName}</span>
      <br />
      <span><b>Age</b>: {age}</span>
      <br />
      <span><b>Email</b>: {email.length > 0 ? email : 'Not Available'}</span>
      <br />
      <span><b>Country</b>: {country.length > 0 ? country : 'Not Available'}</span>
      <hr />
    </div>
  )
}

function App() {
  const [contacts, setContacts] = React.useState(contactList)


  return (
    <>
      <h1
        style={{
          textAlign: 'center'
        }}
      >
        Contact List
        </h1>
      <div
        style={{
          margin: '0 auto',
          width: 400,
          backgroundColor: '#eee',
          borderRadius: 4,
          padding: 20,
        }}
      >
        <ul style={{ listStyle: 'none', paddingLeft: 0 }}>
          {contacts.map(item => (
            <li key={item.id}>
              <Contact
                name={item.name}
                lastName={item.lastName}
                age={item.age}
                email={item.email}
                country={item.country}
              />
            </li>
          ))}
        </ul>
      </div>
    </>
  )
}


ReactDOM.render(
  <App />,
  document.getElementById('root')
);
